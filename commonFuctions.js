function addDataSourceToSelectElement(paramSelectElement, paramDataSource, paramValue, paramText) {
	for (let index = 0; index < paramDataSource.length; index++) {
		const element = paramDataSource[index];
		//console.log(element);
		var newSelectOption = document.createElement("option");

		var optionValue = element[paramValue];
		//console.log(optionValue);
		newSelectOption.value = element[paramValue];
		newSelectOption.text = element[paramText];
		paramSelectElement.appendChild(newSelectOption);
	}
}

function isNullOrEmpty(input) {
	if (input == null || input.trim().length == 0) {
		return true;
	}

	return false;
}


function addDataRowToTable(body, values) {
	const NEW_ROW_INDEX = -1;
	var newRow = body.insertRow(NEW_ROW_INDEX);

	for (let i = 0; i < values.length; i++) {
		var cell = newRow.insertCell(i);
		cell.innerHTML = values[i];
	}
}


